import os
from flask import Flask, jsonify, make_response, request, redirect
from classifier import *
from flask_cors import CORS

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
cors = CORS(app, resources={r'/*': {"origins": '*'}})

@app.route('/sentiment', methods=['GET', 'POST'])

def sentiment_analysis():
    if request.method == 'GET':
        text = request.args.get('text')
        if text:
            #LLamar a libreria python para reconocimiento de texto
            clf = SentimentClassifier()
            result = '%.5f' % clf.predict(text)
            return make_response(jsonify({'sentiment': result, 'text': text, 'status_code':200}), 200)
        return make_response(jsonify({'error':'sorry! unable to parse', 'status_code':500}), 500)

if __name__ == '__main__':
   app.run()

