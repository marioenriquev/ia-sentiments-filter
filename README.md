# IA sentiments filter

**Proyecto de Inteligencia Artificial: Filtro de cadenas de texto según emociones.** <br><br>
Este es un clasificador de análisis de emociones y sentimientos pre-entrenado en idioma español. El programa es capaz de decodificar e interpretar una frase, parrafo u oración y luego determinar que tan positiva o negativa es la información ingresada. <br><br>
**Requisitos:**<br>
* Python 3 o superior (con variable de entorno creada)
* Node.js
* Gestor de pauetes pip para Python (viene con el instalador de Python para windows)
* libreria Flask (Instalar utilizando pip con el comando `pip install Flask`)
* libreria Flask-Cors para permitir origin cruzado de peticiones HTTP al servidor (Instalar utilizando pip con el comando `pip install -U flask-cors`) <br><br>

**Instalación:**<br>
1. Instalar la librería de filtros proporcionada por el usuario de gitHub aylliote repo:`https://github.com/aylliote/senti-py`, instalar utilzando el comando `pip install spanish_sentiment_a`
2. Ejecutar el archivo backend/filterService.py con el comando `python filterService.py` y esperar unos segundos, el backend será levantado en: `http://127.0.0.1:5000/`
3. Instalar el frontend ingresando a frontend/ y en la terminal utilizar el comando `npm install`
4. Ejecutar el cliente ingresando el comando `npm run serve` en la carpeta frontend/, el front end se levantará en: `http://localhost:8080/`

**Uso:**<br>
1. Ingresar el texto a evaluar en el TextArea
2. Presionar el botón "Evaluemos el texto"
3. Esperar la respuesta del servidor y divertirse un rato :D







